Exercises
========

.. toctree::
   :caption: All Exercises Content
   :maxdepth: 2

   exercise_guide.rst
   03_exercises.rst
   04_exercises.rst
   05_exercises.rst
   06_exercises.rst
   07_exercises.rst
   08_exercises.rst
   09_exercises.rst
   10_exercises.rst
   11_exercises.rst

