Lecture 01
==========

- int, float
- operator: ``+ - * / ** // %``
- boolean operations, boolean type
- assign variable
- type() function
- print
- if statement
- string with single quote and double quote
- error understanding, define
- indentation
- float precision
