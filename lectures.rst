All Lecture
===============
.. toctree::
   :caption: All Lecture Content
   :maxdepth: 2

   01_lecture.rst
   03_lecture.rst
   04_lecture_dict_list_branch.rst
   05_lecture_func_mod_file.rst
   06_lecture_dict_exception.rst
   07_lecture_module_class.rst
   08_lecture_stdlib_os_sys_spr.rst
   09_lecture_spr_argp_log.rst
   10_lecture_pip_venv_requets.rst
   11_lecture_api_flask.rst
   12_lecture_theend.rst
