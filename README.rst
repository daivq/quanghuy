PyFML1508
=========

Started at 2015 Aug 11.
So-called PyFML2 - the 2nd generation of PyFML.

Long live the http://www.familug.org !

Mentor
------

Viet Hung Nguyen
~~~~~~~~~~~~~~~~

- Nick: HVN, hvnsweeting
- DOB: 1990 Feb 09 :aquarius:
- From: Dien Bien Phu city, Dien Bien, Viet Nam
- Email: hvn@familug.org OR hvnsweeting@gmail.com
- Gitlab: hvn_familug
- Skype: hvnsweeting
- Facebook: hvnsweeting
- Anything else: hvnsweeting
- Homepage: http://www.familug.org
- Github: http://github.com/hvnsweeting

Teaching assistants
-------------------

Tran Tuan Anh
~~~~~~~~~~~~~

- Nick(Skype, FB, blabla...): trananhkma
- DOB: 1991 Nov 11 :scorpius:
- From: Ha Long city, Quang Ninh, Viet Nam
- Email: trananhkma@gmail.com
- Github: http://github.com/trananhkma

Nguyen Thanh Tung
~~~~~~~~~~~~~~~~~

- Nick: kinhvan017
- DOB: 1991 Dec 20
- From: Nghe An, Viet Nam
- Email: nthanhtung91@gmail.com
- Gitlab: kinhvan017
- Skype: kinhvan017
- Github: http://github.com/kinhvan017

